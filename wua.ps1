
set-executionpolicy remotesigned

$bSUCCESS  = 1
$bFAILURE = 0 
$urlWsussn2 = "http://go.microsoft.com/fwlink/?LinkID=74689"
$localFileLocation =  "C:\school\wua\wsusscn2.cab"
$localFileBkLocation = "C:\school\wua\bk\wsusscn2.cab"

function ChangeToRoot
{
   Write-Host "Change Folder to Root"
   cd "C:\"
}

function MBSADownloadCabFile
{
    $fileDownloaded = $bFAILURE
    Write-Host "IN MBSADownloadCabFile"
	Write-Host "Starting download process..."
    $client = new-object System.Net.WebClient
    $CabURL = $urlWsussn2
	
    $downloadLocation = $localFileLocation
    $client.DownloadFile($CabURL,$downloadLocation)
    
    if (Test-Path $localFileLocation)
    {
        Write-Host "wsusscn2.cab downloaded successfully"
        $fileDownloaded = $bSUCCESS
    }
    else
    {
        Write-Host "wsusscn2.cab - error downoading file"
        $fileDownloaded = $bFAILURE
    }
    Write-Host "OUT MBSADownloadCabFile"
    return $fileDownloaded
}

function ProcessMoveFile
{
    Write-Host "IN ProcessMoveFile"
    if (Test-Path $localFileLocation)
    {    
        $fn = "wsusscn2_"
        $FileExt = ".cab"
        $Datetime = Get-Date -f yyyy-MM-dd-HH-mmm

        Move-Item $localFileLocation $localFileBkLocation 
        Rename-Item -Path $localFileBkLocation -NewName ($fn+$Datetime+$FileExt)
    }
    Write-Host "OUT ProcessMoveFile"
}




function ProcessDeleteArchive
{
    $bExist = $bFAILURE
    Write-Host "IN ProcessDeleteArchive"
    if (Test-Path C:\school\wua\bk)
    {
      $bExist =  $bSUCCESS   
    }
    Write-Host "OUT ProcessDeleteArchive"
    return $bExist
}

function MBSAScan
{
    Write-Host "IN RunMBSAScan"
    cd "C:\"
    cd "C:\Program Files\Microsoft Baseline Security Analyzer 2"
    Start-Process mbsacli /catalog "c:\school\wsusscn2.cab" /xmlout>c:\school\wua\out.txt 
    Write-Host "OUT RunMBSAScan"
}


Write-Host "CHECKING INTERNET CONNECTION..."
ChangeToRoot
$ConnectionExist = Test-Connection www.cnn.com -Quiet

if ($ConnectionExist)
{
    Write-Host "Processing..."
    ProcessDeleteArchive
    ProcessMoveFile
    if (MBSADownloadCabFile == $bSUCCESS)
    {
        #Write-Host "Starting Scanning..."
       #MBSAScan
    }
}
else
{
    Write-Host "Error..."
}

Write-Host "BYE..."